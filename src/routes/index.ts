import { Router } from 'express';

import candidateRoutes from './candidate';
import companyRoutes from './company';



const rootRouter: Router = Router();

rootRouter.use('/candidate', candidateRoutes);
rootRouter.use('/company', companyRoutes);


export default rootRouter;
