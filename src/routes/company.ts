import { Router } from 'express';

import { addCompany, getAllCompany, updateCompany, deleteCompany, getCompanyById } from "../controllers/company";

const companyRoutes: Router = Router();

companyRoutes.post('/addCompany', addCompany);

companyRoutes.get('/getAllCompany', getAllCompany);

companyRoutes.get('/:id', getCompanyById);

companyRoutes.put('/updateCompany/:id', updateCompany);

companyRoutes.delete('/deleteCompany/:id', deleteCompany);

export default companyRoutes;
