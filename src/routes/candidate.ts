import { Router } from 'express';

import { addCandidate, getAllCandidates, updateCandidate, deleteCandidate,getCandidateById } from "../controllers/candidate";

const candidateRoutes: Router = Router();

candidateRoutes.post('/addCandidate', addCandidate);
candidateRoutes.get('/getAllCandidates', getAllCandidates);
candidateRoutes.get('/:id', getCandidateById);
candidateRoutes.put('/updateCandidate/:id', updateCandidate);
candidateRoutes.delete('/deleteCandidate/:id', deleteCandidate);

export default candidateRoutes;
