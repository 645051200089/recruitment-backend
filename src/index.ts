import express, { Express } from 'express';
import { PORT } from './secrets';
import rootRouter from './routes';
import companyRoutes from './routes/company';
import { PrismaClient } from '@prisma/client';
const cors = require('cors');

const app: Express = express();


app.use(cors({ origin: "*", credentials: true }));
app.use(express.json());

app.use('/api', rootRouter);
app.use('/api', companyRoutes);
export const prismaClient = new PrismaClient()

app.listen(PORT, () => { console.log('App working!')})
