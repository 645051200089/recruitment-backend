import { Request, Response, NextFunction } from "express";
import { prismaClient } from "..";
import createCompanySchema from "../schema/company";

export const addCompany = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const validatedData = createCompanySchema.parse(req.body);
        const { name, address, email, phone } = validatedData;

        const newCompany = await prismaClient.company.create({
            data: {
                name,
                address,
                email,
                phone
            }
        });

        res.status(201).json(newCompany);
    } catch (error) {
        next(error);
    }
};

export const getAllCompany = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const company = await prismaClient.company.findMany();
        res.status(200).json(company);
    } catch (error) {
        next(error);
    }
};  

export const updateCompany = async (req: Request, res: Response) => {
    try {
        const companyId = parseInt(req.params.id); // Assuming candidateId is passed in the URL params
        const { name, address, email, phone } = req.body;

        // Check if the candidate exists
        const company = await prismaClient.company.findUnique({
            where: { id: companyId },
        });

        if (!company) {
            return res.status(404).json({ error: "Company not found" });
        }

        // Update candidate
        const updateCompany = await prismaClient.company.update({
            where: { id: companyId },
            data: {
                name,
                address,
                email,
                phone
            },
        });

        res.status(200).json(updateCompany);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Internal Server Error" });
    }
};

export const deleteCompany = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const companyId = parseInt(req.params.id);

        const company = await prismaClient.company.findUnique({
            where: { id: companyId },
        });

        if (!company) {
            return res.status(404).json({ error: "Company not found" });
        }

        await prismaClient.company.delete({
            where: { id: companyId },
        });

        res.status(204).send();
    } catch (error) {
        next(error);
    }
};

export const getCompanyById = async (req: Request, res: Response) => {
    const companyId = parseInt(req.params.id)
    try {
        const company = await prismaClient.company.findFirst({
            where : {id : companyId}
        });
        res.status(200).json(company);
    } catch (error) {
       
    }
}
