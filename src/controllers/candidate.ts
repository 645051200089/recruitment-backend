import { Request, Response, NextFunction } from "express";
import { prismaClient } from "..";
import createCandidateSchema from "../schema/candidate";

export const addCandidate = async (req: Request, res: Response, next: NextFunction) => {
    try {

        if (typeof req.body.age === 'string') {
            req.body.age = Number(req.body.age);
        }

        const {status = 'ACTIVE'} = req.body
        const validatedData = createCandidateSchema.parse(req.body);
        const { firstName, surname, nickname, age, address, position, phone } = validatedData;

        const newCandidate = await prismaClient.candidate.create({
            data: {
                firstName,
                surname,
                nickname,
                age,
                status,
                address,
                position,
                phone
            }
        });
        
        res.status(201).json(newCandidate);
    } catch (error) {
        next(error);
    }
};

export const getAllCandidates = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const candidates = await prismaClient.candidate.findMany({where : {status : "ACTIVE"} });
        res.status(200).json(candidates);
    } catch (error) {
        next(error);
    }
};  

export const updateCandidate = async (req: Request, res: Response) => {
    try {
        const candidateId = parseInt(req.params.id); // Assuming candidateId is passed in the URL params\
        req.body.age = Number(req.body.age);
        const { firstName, surname, nickname, age, address, position, phone } = req.body;
    
        // Check if the candidate exists
        const candidate = await prismaClient.candidate.findUnique({
            where: { id: candidateId },
        });

        if (!candidate) {
            return res.status(404).json({ error: "Candidate not found" });
        }

        // Update candidate
        const updatedCandidate = await prismaClient.candidate.update({
            where: { id: candidateId },
            data: {
                firstName,
                surname,
                nickname,
                age,
                address,
                position,
                phone
            },
        });

        res.status(200).json(updatedCandidate);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Internal Server Error" });
    }
};

export const deleteCandidate = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const candidateId = parseInt(req.params.id); // Assuming candidateId is passed in the URL params

        // Check if the candidate exists
        const candidate = await prismaClient.candidate.findUnique({
            where: { id: candidateId },
        });

        if (!candidate) {
            return res.status(404).json({ error: "Candidate not found" });
        }

        // Delete the candidate
        await prismaClient.candidate.update({
            data : {
                status : "INACTIVE"
            },
            where: { id: candidateId },
        });

        res.status(204).send(); // Successfully deleted, return empty response
    } catch (error) {
        next(error);
    }
};

export const getCandidateById = async (req: Request, res: Response) => {
    const candidateId = parseInt(req.params.id)
    try {
        const candidates = await prismaClient.candidate.findFirst({
            where : {id : candidateId}
        });
        res.status(200).json(candidates);
    } catch (error) {
       
    }
}