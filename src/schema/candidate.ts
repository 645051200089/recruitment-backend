import { object, string, number } from 'zod';

const createCandidateSchema = object({
    firstName: string(),
    surname: string(),
    nickname: string(),
    age: number(),
    address: string(),
    position: string(),
    phone: string(),
});

export default createCandidateSchema;
