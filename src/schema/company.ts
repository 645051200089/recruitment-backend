import { object, string, } from 'zod';

const createCompanySchema = object({
  name: string(),
  address: string(),
  email: string(),
  phone: string()
});

export default createCompanySchema;
